module.exports = {
  extends: 'erb',
  settings: {
    'import/resolver': {
      webpack: {
        config: require.resolve('./configs/webpack.config.eslint.js')
      }
    }
  },
  rules: {
    'react/jsx-props-no-spreading': 'off',
    'react/jsx-curly-brace-presence': 'off',
    'react/self-closing-comp': 'off',
    'react/forbid-prop-types': 'off',
    'jsx-a11y/media-has-caption': 'off'
  }
};
