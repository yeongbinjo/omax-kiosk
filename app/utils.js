import { app } from 'electron';
import { join } from 'path';

const signatures = [
  'W9Y7M80DWL2NLHXIX65N',
  'PWSPMSVNR5A300W2ZAE7',
  'XX43O8IWNZT4595XCGNH'
];

const reverse = str =>
  Array.from(str)
    .reverse()
    .join('');

export const getId = l => {
  // eslint-disable-next-line no-param-reassign
  if (!l) l = 9;
  return `_${Math.random()
    .toString(36)
    .substr(2, l)}`;
};

export const getItemPath = (itemId, type) => {
  const basePath = app.getPath('appData');
  if (type === 0) {
    return join(basePath, signatures[0], reverse(itemId));
  }
  if (type === 1) {
    return join(basePath, signatures[0], reverse(itemId), signatures[1]);
  }
  if (type === 2) {
    return join(basePath, signatures[0], reverse(itemId), signatures[2]);
  }
  return null;
};
