/* eslint global-require: off */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `yarn build` or `yarn build-main`, this file is compiled to
 * `./app/main.prod.js` using webpack. This gives us some performance wins.
 *
 * @flow
 */
import { app, BrowserWindow, ipcMain } from 'electron';
import { autoUpdater } from 'electron-updater';
import { copyFileSync, existsSync, mkdirSync } from 'fs';
import { spawn } from 'child_process';
import log from 'electron-log';
import ElectronStore from 'electron-store';
import { sha256 } from 'js-sha256';
import { join } from 'path';
import MenuBuilder from './menu';
import { getItemPath } from './utils';

// const youtubedl = require('youtube-dl');
const fs = require('fs');
const axios = require('axios');
const ytdl = require('ytdl-core');

export default class AppUpdater {
  constructor() {
    log.transports.file.level = 'info';
    autoUpdater.logger = log;
    autoUpdater.checkForUpdatesAndNotify();
  }
}

let mainWindow = null;

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support');
  sourceMapSupport.install();
}

if (
  process.env.NODE_ENV === 'development' ||
  process.env.DEBUG_PROD === 'true'
) {
  require('electron-debug')();
}

const installExtensions = async () => {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ['REACT_DEVELOPER_TOOLS', 'REDUX_DEVTOOLS'];

  return Promise.all(
    extensions.map(name => installer.default(installer[name], forceDownload))
  ).catch(console.log);
};

/**
 * Add event listeners...
 */

app.on('window-all-closed', () => {
  app.quit();
});

app.on('ready', async () => {
  if (
    process.env.NODE_ENV === 'development' ||
    process.env.DEBUG_PROD === 'true'
  ) {
    await installExtensions();
  }

  const defaultOptions = {
    show: false,
    width: 1920,
    height: 1080,
    frame: false,
    webPreferences: {
      nodeIntegration: true
    }
  };
  mainWindow = new BrowserWindow(defaultOptions);

  mainWindow.loadURL(`file://${__dirname}/app.html`);

  // @TODO: Use 'ready-to-show' event
  //        https://github.com/electron/electron/blob/master/docs/api/browser-window.md#using-ready-to-show-event
  mainWindow.webContents.on('did-finish-load', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    if (process.env.START_MINIMIZED) {
      mainWindow.minimize();
    } else {
      mainWindow.show();
      mainWindow.focus();
    }
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();

  // Remove this if your app does not use auto updates
  // eslint-disable-next-line
  // new AppUpdater();

  if (store.get('config.kioskMode')) {
    mainWindow.setKiosk(true);
  }
});

ipcMain.on('toggle-kiosk-mode', () => {
  if (mainWindow) {
    mainWindow.setKiosk(!mainWindow.isKiosk());
  }
});

ipcMain.on('set-kiosk-mode', (_, arg) => {
  if (mainWindow) {
    mainWindow.setKiosk(arg);
  }
});

// eslint-disable-next-line no-unused-vars
ipcMain.on('get-kiosk-mode', (event, arg) => {
  // eslint-disable-next-line no-param-reassign
  event.returnValue = mainWindow && mainWindow.isKiosk();
});

ipcMain.on('save-config', (event, arg) => {
  store.set(arg.name, arg.value);
});

ipcMain.on('load-config', (event, arg) => {
  // eslint-disable-next-line no-param-reassign
  event.returnValue = store.get(arg);
});

ipcMain.on('quit-app', () => {
  app.quit();
});

ipcMain.on('quit-desktop', () => {
  // build argumnets
  // execute the shutdown command
  console.log(process.platform);
  if (process.platform === 'win32') {
    spawn('shutdown', ['-s', '-f', '-t', 30]);
  } else {
    app.quit();
  }
});

ipcMain.on('set-open-at-login', () => {
  app.setLoginItemSettings({
    openAtLogin: true
  });
});

ipcMain.on('unset-open-at-login', () => {
  app.setLoginItemSettings({
    openAtLogin: false
  });
});

ipcMain.on('get-item', (event, arg) => {
  const items = store.get('items');
  // eslint-disable-next-line no-restricted-syntax
  for (const item of items) {
    if (item.id === arg) {
      // eslint-disable-next-line no-param-reassign
      event.returnValue = item;
      return;
    }
  }
  // eslint-disable-next-line no-param-reassign
  event.returnValue = null;
});

ipcMain.on('add-item', (event, arg) => {
  let success = true;

  if (!existsSync(getItemPath(arg.id, 0))) {
    mkdirSync(getItemPath(arg.id, 0), { recursive: true });
  }

  try {
    copyFileSync(arg.item, getItemPath(arg.id, 1));
    copyFileSync(arg.thumbnail, getItemPath(arg.id, 2));
  } catch (err) {
    console.log(err);
    success = false;
  }

  if (success) {
    const items = store.get('items') ? store.get('items') : [];
    store.set('items', [
      {
        id: arg.id,
        category: arg.category,
        description: arg.description,
        title: arg.title
      },
      ...items
    ]);
  }

  event.reply('add-item-reply', success);
});

ipcMain.on('edit-item', (event, arg) => {
  let success = true;

  const items = store.get('items') ? store.get('items') : [];
  let i;
  for (i = 0; i < items.length; i += 1) {
    if (items[i].id === arg.id) {
      break;
    }
  }

  if (i >= items.length) {
    success = false;
  }

  if (!existsSync(getItemPath(arg.id, 0))) {
    mkdirSync(getItemPath(arg.id, 0), { recursive: true });
  }

  if (arg.item) {
    try {
      copyFileSync(arg.item, getItemPath(arg.id, 1));
    } catch (err) {
      console.log(err);
      success = false;
    }
  }

  if (arg.thumbnail) {
    try {
      copyFileSync(arg.thumbnail, getItemPath(arg.id, 2));
    } catch (err) {
      console.log(err);
      success = false;
    }
  }

  if (success) {
    items[i] = {
      id: arg.id,
      category: arg.category,
      description: arg.description,
      title: arg.title
    };
    store.set('items', items);
  }

  event.reply('edit-item-reply', success);
});

ipcMain.on('delete-item', (event, arg) => {
  let success = true;
  const items = store.get('items') ? store.get('items') : [];
  let i;
  for (i = 0; i < items.length; i += 1) {
    if (items[i].id === arg) {
      break;
    }
  }
  if (i < items.length) {
    items.splice(i, 1);
    store.set('items', items);
  } else {
    success = false;
  }
  event.returnValue = success;
});

ipcMain.on('get-item-path', (event, arg) => {
  // eslint-disable-next-line no-param-reassign
  event.returnValue = getItemPath(arg.itemId, arg.type);
});

ipcMain.on('get-category-name', (event, arg) => {
  const categories = store.get('categories');
  // eslint-disable-next-line no-restricted-syntax
  for (const category of categories) {
    if (category.id === arg) {
      // eslint-disable-next-line no-param-reassign
      event.returnValue = category.name;
      return;
    }
  }
  // eslint-disable-next-line no-param-reassign
  event.returnValue = null;
});

ipcMain.on('login', (event, arg) => {
  // eslint-disable-next-line no-param-reassign
  event.returnValue = store.get('config.password') === sha256(arg);
});

ipcMain.on('change-password', (event, arg) => {
  store.set('config.password', sha256(arg));
  // eslint-disable-next-line no-restricted-globals
  event.returnValue = true;
});

ipcMain.on('reset-config', event => {
  store.set('config', defaultConfig);
  if (mainWindow && mainWindow.isKiosk()) {
    mainWindow.setKiosk(false);
  }
  event.returnValue = true;
});

ipcMain.on('add-view-count', (_, arg) => {
  const viewCount = store.get(`viewCounts.${arg}`);
  store.set(
    `viewCounts.${arg}`,
    viewCount && typeof viewCount === 'number' ? viewCount + 1 : 1
  );
});

ipcMain.on('get-view-count', (event, arg) => {
  const viewCount = store.get(`viewCounts.${arg}`);
  event.returnValue =
    viewCount && typeof viewCount === 'number' ? viewCount : 0;
});

ipcMain.on('set-bookmark', (event, arg) => {
  store.set(`bookmarks.${arg}`, true);
  event.returnValue = true;
});

ipcMain.on('unset-bookmark', (event, arg) => {
  const bookmarks = store.get('bookmarks');
  if (typeof bookmarks[arg] !== 'undefined') {
    delete bookmarks[arg];
    store.set(`bookmarks`, bookmarks);
  }

  event.returnValue = true;
});

ipcMain.on('get-bookmark', (event, arg) => {
  const bookmark = store.get(`bookmarks.${arg}`);
  event.returnValue = !!bookmark;
});

ipcMain.on('add-comment', (event, arg) => {
  const comments = store.get('comments');
  if (typeof comments[arg.itemId] !== 'undefined') {
    comments[arg.itemId] = [arg.comment, ...comments[arg.itemId]];
  } else {
    comments[arg.itemId] = [arg.comment];
  }
  store.set('comments', comments);
  event.returnValue = true;
});

ipcMain.on('edit-comment', (event, arg) => {
  let success = true;
  const comments = store.get('comments');
  if (
    typeof comments[arg.itemId] !== 'undefined' &&
    arg.idx < comments[arg.itemId].length &&
    comments[arg.itemId][arg.idx].password === arg.comment.password
  ) {
    comments[arg.itemId].splice(arg.idx, 1, arg.comment);
  } else {
    success = false;
  }
  store.set('comments', comments);
  event.returnValue = success;
});

ipcMain.on('delete-comment', (event, arg) => {
  let success = true;
  const comments = store.get('comments');
  if (
    typeof comments[arg.itemId] !== 'undefined' &&
    arg.idx < comments[arg.itemId].length
  ) {
    comments[arg.itemId].splice(arg.idx, 1);
  } else {
    success = false;
  }
  store.set('comments', comments);
  event.returnValue = success;
});

ipcMain.on('get-comments', (event, arg) => {
  const comments = store.get(`comments.${arg}`);
  event.returnValue = comments || [];
});

ipcMain.on('get-all-comments', event => {
  const comments = store.get(`comments`);
  let result = [];
  for (const [key, value] of Object.entries(comments)) {
    result = result.concat(
      value.map((v, i) => {
        return {
          ...v,
          itemId: key,
          idx: i
        };
      })
    );
  }
  result.sort((l, r) => {
    const ld = Number(l.datetime);
    const rd = Number(r.datetime);
    if (ld < rd) return 1;
    if (ld > rd) return -1;
    return 0;
  });
  event.returnValue = result;
});

ipcMain.on('get-youtube', (event, arg) => {
  if (!arg || !arg.url) {
    event.reply('get-youtube-reply', {
      success: false
    });
  }

  // https://gist.github.com/Marco01809/34d47c65b1d28829bb17c24c04a0096f
  /*
  const ydl = youtubedl(arg.url, ['--format=22']);

  ydl.on('error', err => {
    event.reply('get-youtube-reply', {
      success: false,
      error: err,
    });
  });

  ydl.on('info', info => {
    ydl.pipe(
      fs.createWriteStream(join(app.getPath('downloads'), `${info.id}.mp4`))
    );
  });

  ydl.on('end', () => {
    youtubedl.getInfo(arg.url, [], (err, info) => {
      const thumbnail = join(app.getPath('downloads'), `${info.id}.jpeg`);
      const video = join(app.getPath('downloads'), `${info.id}.mp4`);

      if (err) {
        event.reply('get-youtube-reply', {
          success: false,
          error: err
        });
      }

      axios({
        method: 'get',
        url: info.thumbnail,
        responseType: 'stream'
      }).then(response => {
        response.data
          .pipe(fs.createWriteStream(thumbnail))
          .on('finish', () => {
            event.reply('get-youtube-reply', {
              success: true,
              id: info.id,
              title: info.title,
              description: info.description,
              video,
              thumbnail
            });
          })
          .on('error', () => {
            event.reply('get-youtube-reply', {
              success: false,
              error: 'thumbnail'
            });
          });
      });
    });
  });
  */

  const ydl = ytdl(arg.url);

  ydl.on('error', err => {
    event.reply('get-youtube-reply', {
      success: false,
      error: err
    });
  });

  ydl.on('info', info => {
    ydl.pipe(
      fs.createWriteStream(
        join(app.getPath('downloads'), `${info.video_id}.mp4`)
      )
    );
  });

  ydl.on('end', () => {
    ytdl.getInfo(arg.url, [], (err, info) => {
      const { thumbnails } = info.player_response.videoDetails.thumbnail;
      const thumbnailUrl = thumbnails[thumbnails.length - 1].url;
      const thumbnail = join(app.getPath('downloads'), `${info.video_id}.jpeg`);
      const video = join(app.getPath('downloads'), `${info.video_id}.mp4`);

      if (err) {
        event.reply('get-youtube-reply', {
          success: false,
          error: err
        });
      }

      axios({
        method: 'get',
        url: thumbnailUrl,
        responseType: 'stream'
      })
        .then(response => {
          response.data
            .pipe(fs.createWriteStream(thumbnail))
            .on('finish', () => {
              event.reply('get-youtube-reply', {
                success: true,
                id: info.video_id,
                title: info.title,
                description: info.description,
                video,
                thumbnail
              });
            })
            .on('error', () => {
              event.reply('get-youtube-reply', {
                success: false,
                error: 'thumbnail'
              });
            });
        })
        .catch(() => {
          event.reply('get-youtube-reply', {
            success: false,
            error: 'axios'
          });
        });
    });
  });
});

const defaultConfig = {
  kioskMode: false,
  password: '42e50fee71bddb404a920ff5a9abb20e9a4c6925645677d4fe5e9d2e8bdeebe7'
};

const schema = {
  config: {
    type: 'object',
    default: defaultConfig
  },
  categories: {
    type: 'array',
    default: []
  },
  items: {
    type: 'array',
    default: []
  },
  viewCounts: {
    type: 'object',
    default: {}
  },
  bookmarks: {
    type: 'object',
    default: {}
  },
  comments: {
    type: 'object',
    default: {}
  }
};
const store = new ElectronStore({ schema });
