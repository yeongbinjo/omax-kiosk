import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes';
import App from './containers/App';
import RootPage from './containers/RootPage';
import AdminPage from './containers/AdminPage';
import CounterPage from './containers/CounterPage';

export default () => (
  <App>
    <Switch>
      <Route path={routes.COUNTER} component={CounterPage} />
      <Route path="/admin/:menu" component={AdminPage} />
      <Route path="/" component={RootPage} />
    </Switch>
  </App>
);
