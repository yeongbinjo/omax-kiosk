import React, { useState } from 'react';
import clsx from 'clsx';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { Link, useParams } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import AdminLoginDialog from './AdminLoginDialog';
import HiddenBar from './HiddenBar';
import AdminGeneral from './AdminGeneral';
import AdminCategory from './AdminCategory';
import AdminItem from './AdminItem';
import Logo from '../assets/images/omax_logo.png';
import AdminItems from './AdminItems';
import AdminComments from './AdminComments';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    backgroundColor: '#232c39',
    backgroundImage: `linear-gradient(
      135deg,
      rgba(77, 77, 77, 0.63) 10%,
      rgba(18, 20, 36, 0.7)
    )`
  },
  toolbar: {
    paddingLeft: theme.spacing(4),
    paddingRight: theme.spacing(4),
    '& a': {
      color: '#fff',
      textDecoration: 'none'
    }
  },
  toolbarLogo: {
    '-webkit-app-region': 'drag',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: 'none'
  },
  titleMenu: {
    marginRight: theme.spacing(5),
    fontFamily: "'NanumSquare', sans-serif",
    fontSize: '1.2rem',
    fontWeight: 900,
    color: 'rgba(235,240,255,0.9)'
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start'
  },
  container: {
    position: 'relative',
    padding: theme.spacing(4),
    flex: '1 1 auto'
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column'
  },
  toolbarSpacer: { flexGrow: 1 },
  sideMenu: {
    '& li': {
      '& a': {
        width: '100%'
      }
    }
  },
  gridList: {
    '& .MuiGridListTile-tile': {
      position: 'relative',
      '& > a': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%'
      }
    },
    '& .MuiGridListTileBar-rootSubtitle': {
      height: 80
    },
    '& .MuiGridListTileBar-title': {
      fontWeight: 700,
      lineHeight: '1.3rem',
      maxHeight: '2.6rem',
      WebkitLineClamp: 2,
      whiteSpace: 'normal'
    },
    '& .MuiGridListTileBar-root': {
      backgroundColor: 'rgba(0,0,0,0.7)'
    },
    '& .MuiGridListTileBar-subtitle': {
      marginTop: '0.3rem'
    }
  }
}));

const StyledButton = withStyles({
  root: {
    width: '100%',
    height: 60,
    '&:hover, &:active': {
      color: `hsl(223, 65%, 39%)`
    }
  },
  label: {
    fontFamily: '"NanumSquare", sans-serif',
    fontWeight: 900,
    fontSize: '1.2rem',
    letterSpacing: '1px',
    textShadow: '0.05em 0.05em 0 #fff, 0.1em 0.1em 0 #aaa',
    transition: 'color .2s'
  }
})(Button);

export default function Admin() {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const { menu } = useParams();

  const handleClose = () => {
    setOpen(false);
  };

  const openAdminDialog = () => {
    setOpen(true);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="absolute"
        className={clsx(classes.appBar, classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <Link to="/">
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              className={classes.titleMenu}
            >
              홈 화면으로 돌아가기
            </Typography>
          </Link>
          <div className={classes.toolbarSpacer} />
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper)
        }}
      >
        <div className={classes.toolbarLogo}>
          <img src={Logo} alt="omax logo" />
        </div>
        <Divider />
        <List className={classes.sideMenu}>
          <ListItem>
            <Link to="/admin/general">
              <StyledButton variant="contained">일반</StyledButton>
            </Link>
          </ListItem>
          <ListItem>
            <Link to="/admin/category">
              <StyledButton variant="contained">카테고리 관리</StyledButton>
            </Link>
          </ListItem>
          <ListItem>
            <Link to="/admin/item">
              <StyledButton variant="contained">영상 등록</StyledButton>
            </Link>
          </ListItem>
          <ListItem>
            <Link to="/admin/items">
              <StyledButton variant="contained">영상 관리</StyledButton>
            </Link>
          </ListItem>
          <ListItem>
            <Link to="/admin/comments">
              <StyledButton variant="contained">댓글 관리</StyledButton>
            </Link>
          </ListItem>
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="xl" className={classes.container}>
          {menu === 'general' && <AdminGeneral />}
          {menu === 'category' && <AdminCategory />}
          {menu === 'item' && <AdminItem />}
          {menu === 'items' && <AdminItems />}
          {menu === 'comments' && <AdminComments />}
        </Container>
      </main>

      <AdminLoginDialog open={open} onClose={handleClose} />
      <HiddenBar onSuccess={openAdminDialog} />
    </div>
  );
}
