import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { ipcRenderer } from 'electron';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { yellow } from '@material-ui/core/colors';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles(theme => ({
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  button: {
    marginTop: 24,
    marginBottom: 16,
    height: 40
  },
  passwordForm: {
    display: 'flex',
    flexDirection: 'column',
  },
  divider: {
    opacity: 0,
    marginTop: 8,
    marginBottom: 8,
  }
}));

const YellowTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: yellow[700]
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: yellow[700]
    },
    '& .MuiOutlinedInput-root': {
      '& input': {
        color: '#fed140'
      },
      '& fieldset': {
        borderColor: '#fed140'
      },
      '&:hover fieldset': {
        borderColor: '#3670a0'
      },
      '&.Mui-focused fieldset': {
        borderColor: yellow[700]
      }
    }
  }
})(TextField);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Grow in timeout={1000} ref={ref} {...props} />;
});

AdminChangePasswordDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

export default function AdminChangePasswordDialog({ open, onClose }) {
  const classes = useStyles();
  const [org, setOrg] = useState('');
  const [src, setSrc] = useState('');
  const [dst, setDst] = useState('');

  useEffect(() => {
    setOrg('');
    setSrc('');
    setDst('');
  }, [open]);

  const changePassword = () => {
    if (!src || !dst || !org) {
      onClose('비밀번호를 입력해주세요.');
    } else if (src !== dst) {
      onClose('확인 비밀번호가 다릅니다.');
    } else if (ipcRenderer.sendSync('login', org)) {
      ipcRenderer.sendSync('change-password', src);
      onClose('비밀번호가 성공적으로 변경되었습니다.');
    } else {
      onClose('기존 비밀번호가 다릅니다.');
    }
  };

  return (
    <Dialog
      open={open}
      className={classes.dialog}
      TransitionComponent={Transition}
      onClose={() => onClose('')}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogContent>
        <div className={classes.passwordForm}>
          <YellowTextField
            color="secondary"
            id="outlined-text-input"
            margin="dense"
            label="기존 비밀번호"
            value={org}
            onChange={e => setOrg(e.target.value)}
            type="password"
            variant="outlined"
            name="org"
          />
          <Divider className={classes.divider} />
          <YellowTextField
            color="secondary"
            id="outlined-text-input"
            margin="dense"
            label="새 비밀번호"
            value={src}
            onChange={e => setSrc(e.target.value)}
            type="password"
            variant="outlined"
            name="src"
          />
          <YellowTextField
            color="secondary"
            id="outlined-text-input"
            margin="dense"
            label="새 비밀번호 확인"
            value={dst}
            onChange={e => setDst(e.target.value)}
            type="password"
            variant="outlined"
            name="dst"
          />
          <Button
            variant="outlined"
            color="secondary"
            className={classes.button}
            onClick={changePassword}
          >
            변경
          </Button>
        </div>
      </DialogContent>
    </Dialog>
  );
}
