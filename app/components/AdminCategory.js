import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import EditIcon from '@material-ui/icons/Edit';
import InfoIcon from '@material-ui/icons/Info';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  SortableContainer,
  SortableElement,
  SortableHandle
} from 'react-sortable-hoc';
import { ipcRenderer } from 'electron';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import arrayMove from 'array-move';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import AdminEditCategoryDialog from './AdminEditCategoryDialog';
import { getId } from '../utils';
import Snackbar from "@material-ui/core/Snackbar";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    '& .MuiListItemIcon-root': {
      minWidth: 45
    }
  },
  paper: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 500,
    marginBottom: theme.spacing(2),
    '& .MuiList-root': {
      width: '100%'
    }
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 28,
    margin: 4
  }
}));

const DragHandle = SortableHandle(() => <MenuIcon />);

const SortableItem = SortableElement(({ idx, value, update }) => {
  const [state, setState] = useState({
    open: false
  });

  const onClose = newValue => {
    update(idx, 'edit', newValue);
    setState({ open: false });
  };

  return (
    <ListItem button>
      <ListItemIcon>
        <DragHandle />
      </ListItemIcon>
      <ListItemText primary={value.name} />
      <ListItemIcon onClick={() => setState({ open: true })}>
        <EditIcon />
      </ListItemIcon>
      <ListItemIcon onClick={() => update(idx, 'delete')}>
        <DeleteIcon />
      </ListItemIcon>

      <AdminEditCategoryDialog
        open={state.open}
        text={value.name}
        onClose={onClose}
      />
    </ListItem>
  );
});

const SortableList = SortableContainer(({ items, update }) => {
  return (
    <List aria-label="categories">
      {items.map((value, index) => (
        <SortableItem
          key={`item-${value.id}`}
          index={index}
          idx={index}
          value={value}
          update={update}
        />
      ))}
    </List>
  );
});

export default function AdminCategory() {
  const classes = useStyles();
  const [state, setState] = useState({
    categories: [],
    newCategory: ''
  });
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');

  useEffect(() => {
    setState({
      ...state,
      categories: ipcRenderer.sendSync('load-config', 'categories')
    });
  }, []);

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const saveCategories = cs => {
    ipcRenderer.send('save-config', {
      name: 'categories',
      value: cs
    });
  };

  const onSortEnd = ({ oldIndex, newIndex }) => {
    const newCs = arrayMove(state.categories, oldIndex, newIndex);
    setState({ ...state, categories: newCs });
    saveCategories(newCs);
  };

  const updateCategory = (index, type, arg) => {
    if (type === 'edit' && arg) {
      const newCs = JSON.parse(JSON.stringify(state.categories));
      newCs[index].name = arg;
      setState({ ...state, categories: newCs });
      saveCategories(newCs);
      setSnackbarMessage('카테고리가 수정되었습니다.');
      setSnackbarOpen(true);
    } else if (type === 'delete') {
      const newCs = JSON.parse(JSON.stringify(state.categories));
      newCs.splice(index, 1);
      setState({ ...state, categories: newCs });
      saveCategories(newCs);
      setSnackbarMessage('카테고리가 삭제되었습니다.');
      setSnackbarOpen(true);
    }
  };

  const addCategory = () => {
    if (state.newCategory) {
      const newCs = [
        ...state.categories,
        { name: state.newCategory, id: getId() }
      ];
      setState({
        ...state,
        categories: newCs,
        newCategory: ''
      });
      saveCategories(newCs);
      setSnackbarMessage('카테고리가 등록되었습니다.');
      setSnackbarOpen(true);
    }
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <IconButton className={classes.iconButton} aria-label="menu">
          <InfoIcon />
        </IconButton>
        <InputBase
          className={classes.input}
          placeholder="추가할 카테고리를 입력해주세요"
          inputProps={{ 'aria-label': 'add category' }}
          value={state.newCategory}
          onChange={e => setState({ ...state, newCategory: e.target.value })}
        />
        <IconButton
          type="submit"
          className={classes.iconButton}
          aria-label="add"
          onClick={addCategory}
        >
          <AddCircleOutlineIcon />
        </IconButton>
      </Paper>

      <Paper className={classes.paper}>
        <SortableList
          items={state.categories}
          onSortEnd={onSortEnd}
          update={updateCategory}
          useDragHandle
        />
      </Paper>

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        open={snackbarOpen}
        autoHideDuration={3000}
        onClose={handleSnackbarClose}
        message={<span>{snackbarMessage}</span>}
      />
    </div>
  );
}
