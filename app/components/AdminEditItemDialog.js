import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { FilePond } from 'react-filepond';
import { ipcRenderer } from 'electron';
import styled from 'styled-components';

const useStyles = makeStyles(theme => ({
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  button: {
    marginLeft: theme.spacing(1),
    marginTop: 8,
    marginBottom: 4,
    height: 40
  },
  item: {
    minHeight: '150px',
    display: 'flex',
    flexDirection: 'row',
    '& > div:first-child': {
      flex: '0 1 450px',
      padding: theme.spacing(3),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      '& h3': {
        marginTop: 0
      },
      borderRight: '1px solid rgba(255,255,255,0.3)'
    },
    '& > div:last-child': {
      flex: '1 1 auto',
      padding: theme.spacing(3),
      '& label.Mui-focused': {
        color: 'white'
      },
      '& button:not(.filepond--file-action-button)': {
        minWidth: 160,
        height: 48
      }
    }
  },
  formControl: {
    minWidth: 120
  }
}));

const StyledButton = styled(Button)`
  background: linear-gradient(45deg, #6b98fe 30%, #53ffcb 90%);
  border-radius: 3px;
  border: 0;
  color: black;
  height: 48px;
  padding: 0 30px;
  box-shadow: 0 3px 5px 2px rgba(255, 255, 255, 0.3);
`;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Grow in timeout={1000} ref={ref} {...props} />;
});

AdminEditItemDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  target: PropTypes.object,
  onClose: PropTypes.func.isRequired
};

export default function AdminEditItemDialog({ open, target, onClose }) {
  const classes = useStyles();
  const [state, setState] = useState({
    title: '',
    description: '',
    category: ''
  });
  const [categories, setCategories] = useState([]);
  const [item, setItem] = useState(null);
  const [thumbnail, setThumbnail] = useState(null);
  const [disabled, setDisabled] = useState(false);
  const pond = useRef(null);

  useEffect(() => {
    setCategories(ipcRenderer.sendSync('load-config', 'categories'));

    ipcRenderer.on('edit-item-reply', (_, arg) => {
      setDisabled(false);
      if (arg) {
        onClose('영상 수정을 성공적으로 완료했습니다.');
      } else {
        onClose('영상 수정에 실패하였습니다.');
      }
    });

    return () => {
      ipcRenderer.removeAllListeners('edit-item-reply');
    };
  }, []);

  useEffect(() => {
    if (target) {
      setState({
        title: target.title,
        description: target.description,
        category: target.category
      });
    }
    setItem(null);
    setThumbnail(null);
  }, [target]);

  const handleChange = e => {
    setState({ ...state, [e.target.name]: e.target.value });
  };

  const editItem = () => {
    setDisabled(true);
    ipcRenderer.send('edit-item', {
      ...state,
      item: item ? item.path : null,
      thumbnail: thumbnail ? thumbnail.path : null,
      id: target.id
    });
  };

  return (
    <Dialog
      fullWidth
      maxWidth={'xl'}
      open={open}
      className={classes.dialog}
      TransitionComponent={Transition}
      onClose={() => onClose('')}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogContent>
        <div className={classes.item}>
          <div>
            <h3>영상 제목</h3>
            <span>영상의 제목을 입력하세요.</span>
          </div>
          <div>
            <TextField
              id="filled-full-width"
              label="영상 제목"
              style={{ marginTop: -8 }}
              placeholder="영상 제목"
              fullWidth
              margin="normal"
              variant="filled"
              disabled={disabled}
              value={state.title}
              name="title"
              onChange={handleChange}
            />
          </div>
        </div>
        <div className={classes.item}>
          <div>
            <h3>영상 설명</h3>
            <span>영상의 설명을 입력하세요.</span>
          </div>
          <div>
            <TextField
              id="filled-full-width"
              label="영상 설명"
              style={{ marginTop: -8 }}
              placeholder="영상 설명"
              multiline
              rows="8"
              fullWidth
              margin="normal"
              variant="filled"
              disabled={disabled}
              value={state.description}
              name="description"
              onChange={handleChange}
            />
          </div>
        </div>
        <div className={classes.item}>
          <div>
            <h3>영상 카테고리</h3>
            <span>
              영상의 카테고리를 입력하세요. 카테고리에 대한 설정은{' '}
              <em>카테고리 관리</em>에서 가능합니다.
            </span>
          </div>
          <div>
            <FormControl
              variant="filled"
              className={classes.formControl}
              style={{ marginTop: -8 }}
            >
              <InputLabel id="demo-simple-select-filled-label">
                카테고리
              </InputLabel>
              <Select
                labelId="demo-simple-select-filled-label"
                id="demo-simple-select-filled"
                disabled={disabled}
                value={state.category}
                onChange={handleChange}
                name="category"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {categories.map(c => (
                  <MenuItem key={c.id} value={c.id}>
                    {c.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
        </div>
        <div className={classes.item}>
          <div>
            <h3>영상 파일</h3>
            <span>
              등록할 영상 파일을 우측 영역에 올려주세요. MP4 포맷만 현재
              지원합니다.
            </span>
          </div>
          <div>
            <FilePond
              ref={pond}
              allowMultiple={false}
              maxFiles={1}
              acceptedFileTypes={['video/*']}
              onupdatefiles={fileItems => {
                if (fileItems.length > 0) {
                  setItem(fileItems[0].file);
                }
              }}
            />
          </div>
        </div>
        <div className={classes.item}>
          <div>
            <h3>영상 섬네일</h3>
            <span>영상 목록에 노출될 섬네일 이미지 파일을 등록해주세요.</span>
          </div>
          <div>
            <FilePond
              ref={pond}
              allowMultiple={false}
              maxFiles={1}
              acceptedFileTypes={['image/*']}
              onupdatefiles={fileItems => {
                if (fileItems.length > 0) {
                  setThumbnail(fileItems[0].file);
                }
              }}
            />
          </div>
        </div>
        <div className={classes.item}>
          <div>
            <h3>영상 저장 및 등록</h3>
            <span>
              영상을 지정된 위치에 은닉하고 암호화하여 저장합니다. 등록된 영상은
              바로 게시됩니다.
            </span>
          </div>
          <div>
            <StyledButton disabled={disabled} onClick={editItem}>
              수정
            </StyledButton>
          </div>
        </div>
      </DialogContent>
    </Dialog>
  );
}
