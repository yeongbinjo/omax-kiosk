import React, { useState } from 'react';
import clsx from 'clsx';
import * as moment from 'moment';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';

const useStyles = makeStyles(theme => ({
  fab: {
    zIndex: 1300,
    opacity: 0,
    cursor: 'default',
    width: theme.spacing(10),
    height: theme.spacing(10)
  },
  adminSequenceStartButton: {
    position: 'fixed',
    left: 0,
    top: 0
  },
  adminSequenceEndButton: {
    position: 'fixed',
    right: 0,
    bottom: 100
  }
}));

HiddenBar.propTypes = {
  onSuccess: PropTypes.func.isRequired
};

export default function HiddenBar({ onSuccess }) {
  const classes = useStyles();
  const [adminSequenceStartTime, setAdminSequenceStartTime] = useState(null);
  const [adminSequenceStartCount, setAdminSequenceStartCount] = useState(0);

  // 클라이언트 요청으로 상단 우측 5번을 2초내로 클릭
  const handleAdminSequenceStart = () => {
    const now = moment();
    // 0.4초 이내로 못 눌렀으면 초기화
    if (
      adminSequenceStartTime === null ||
      now.diff(adminSequenceStartTime) > 500
    ) {
      setAdminSequenceStartCount(1);
      // setAdminSequenceEndCount(0);
    } else {
      setAdminSequenceStartCount(adminSequenceStartCount + 1);
    }
    setAdminSequenceStartTime(now);

    // 조건을 만족한 경우 어드민 인증 창을 연다
    if (adminSequenceStartCount === 4) {
      setAdminSequenceStartCount(0);

      onSuccess();
    }
  };

  return (
    <div>
      <Fab
        color="primary"
        aria-label="start-admin-sequence"
        className={clsx(classes.fab, classes.adminSequenceStartButton)}
        onClick={handleAdminSequenceStart}
      >
        <></>
      </Fab>
    </div>
  );
}
