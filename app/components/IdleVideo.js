import React, { useEffect, useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { ipcRenderer } from 'electron';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import videojs from 'video.js';
import PropTypes from 'prop-types';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    zIndex: theme.zIndex.drawer + 2,
    transition: '1s opacity',
    opacity: 1
  },
  videoContainer: {
    position: 'relative',
    padding: '0 !important',
    flex: '1 1 auto',
    display: 'flex',
    height: '100%',
    '& video': {
      width: '100%',
      height: '100%'
    },
    '&::after': {
      content: '""',
      position: 'absolute',
      top: 0,
      left: 500,
      width: 200,
      height: '100%',
      zIndex: theme.zIndex.drawer + 3,
      opacity: 0.8,
      background: 'linear-gradient(to right, #111111, transparent)',
      pointerEvents: 'none'
    }
  },
  detail: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 500,
    height: '100%',
    opacity: 0.8,
    backgroundColor: '#111111',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: theme.spacing(3),
    paddingLeft: theme.spacing(6),
    paddingRight: theme.spacing(4),
    zIndex: theme.zIndex.drawer + 3,
    '& h3': {
      fontSize: '1.8rem',
      lineHeight: '2rem',
      fontWeight: 700
    },
    '& p': {
      fontSize: '1.2rem',
      fontWeight: 500,
      margin: `${theme.spacing(2)}px 0`,
      opacity: 0.7,
      lineHeight: '1.75rem',
      '& span': {
        fontWeight: 800
      }
    },
    pointerEvents: 'none'
  },
  buttons: {
    pointerEvents: 'auto',
    '& button': {
      minWidth: 150,
      margin: theme.spacing(1),
      height: 60,
      fontSize: '1.5rem',
      letterSpacing: 2,
      '& i': {
        marginRight: theme.spacing(1)
      }
    },
    '& button:first-child': {
      marginLeft: 0,
      backgroundColor: '#fe1234',
      color: 'white'
    },
    '& button:last-child': {
      marginRight: 0
    },
    '& .MuiButton-outlined': {
      border: '2px solid rgba(255, 255, 255, 0.8)'
    }
  },
  video: {
    overflow: 'hidden',
    flex: '1 0 auto',
    width: '100%',
    height: '100%',
    zIndex: theme.zIndex.drawer + 2
  },
  transparent: {
    opacity: 0
  },
  hidden: {
    display: 'none !important'
  }
}));

IdleVideo.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
};

IdleVideo.defaultProps = {
  onClose: null
};

export default function IdleVideo({ open, onClose }) {
  const classes = useStyles();
  const [isTransparent, setIsTransparent] = useState(true);
  const [items, setItems] = useState(null);
  const [item, setItem] = useState(null);
  const [isPlaying, setIsPlaying] = useState(true);
  const [isMuted, setIsMuted] = useState(false);

  const player = useRef(null);
  const videoNode = useRef(null);

  useEffect(() => {
    const is = ipcRenderer.sendSync('load-config', 'items');
    if (!is || is.length === 0) {
      return;
    }
    setItems(is);

    player.current = videojs(
      videoNode.current,
      {
        autoplay: false,
        controls: true,
        fluid: true
      },
      function onPlayerReady() {
        console.log('onPlayerReady', this);
      }
    );

    player.current.on('pause', () => {
      setIsPlaying(false);
      // 만약 정지되고 나서 .3초 뒤에도 정지 상태이면 IdleVideo를 종료한다.
      setTimeout(() => {
        if (player.current.paused()) {
          hide();
        }
      }, 300);
    });
    player.current.on('play', () => setIsPlaying(true));
    player.current.on('ended', () => playNext());

    return () => {
      player.current.dispose();
      player.current.off('pause');
      player.current.off('play');
      player.current.off('ended');
    };
  }, []);

  useEffect(() => {
    if (open && player.current) {
      playNext();
      setTimeout(() => setIsTransparent(false), 100);
    }
  }, [open]);

  const playNext = () => {
    if (!player.current) {
      return;
    }
    if (!items || items.length === 0) {
      return;
    }
    const i = items[Math.floor(Math.random() * items.length)];
    setItem(i);

    player.current.src({
      src: ipcRenderer.sendSync('get-item-path', {
        itemId: i.id,
        type: 1
      }),
      type: 'video/mp4'
    });
    player.current.play();
  };

  const hide = () => {
    setIsTransparent(true);
    setTimeout(() => {
      if (onClose) {
        onClose();
      }
    }, 1000);
  };

  const togglePlay = () => {
    if (!player.current) {
      return;
    }
    if (isPlaying) {
      player.current.pause();
    } else {
      // player.current.play();
    }
  };

  const toggleMute = () => {
    if (!player.current) {
      return;
    }
    if (isMuted) {
      player.current.muted(false);
      setIsMuted(false);
    } else {
      player.current.muted(true);
      setIsMuted(true);
    }
  };

  return (
    <div
      className={clsx(
        classes.root,
        !open && classes.hidden,
        isTransparent && classes.transparent
      )}
    >
      <div className={classes.videoContainer}>
        <div className={classes.detail}>
          <Typography component="h3" variant="h1" color="inherit">
            {item ? item.title : ''}
          </Typography>
          <Typography component="p" variant="body1" color="inherit">
            {item ? item.description : ''}
          </Typography>
          <div className={classes.buttons}>
            <Button
              variant="contained"
              color="default"
              onClick={() => togglePlay()}
            >
              {isPlaying ? (
                <>
                  <i className="fas fa-pause" /> 정지
                </>
              ) : (
                <>
                  <i className="fas fa-play" /> 재생
                </>
              )}
            </Button>
            <Button
              variant="contained"
              color="default"
              onClick={() => toggleMute()}
            >
              {isMuted ? (
                <>
                  <i className="fas fa-volume-mute" /> 켜기
                </>
              ) : (
                <>
                  <i className="fas fa-volume-up" /> 끄기
                </>
              )}
            </Button>
          </div>
          <Typography
            component="p"
            variant="body1"
            color="inherit"
            style={{ marginTop: 32 }}
          >
            <span>카테고리:</span>{' '}
            {item
              ? ipcRenderer.sendSync('get-category-name', item.category)
              : ''}
          </Typography>
        </div>
        <div data-vjs-player className={classes.video}>
          <video ref={videoNode} className="video-js vjs-default-skin" />
        </div>
      </div>
    </div>
  );
}
