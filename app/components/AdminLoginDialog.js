import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { ipcRenderer } from 'electron';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import CloseIcon from '@material-ui/icons/Close';
import clsx from 'clsx';
import { yellow } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  passwordForm: {
    '& > button': {
      width: 300
    },
    '& > div': {
      width: 300
    },
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  dialogHeader: {
    margin: `${theme.spacing(1)}px ${theme.spacing(2)}px 0`,
    justifyContent: 'flex-end',
    '& > div': {
      display: 'flex'
    },
    '& > div > button:not(:first-child)': {
      marginLeft: theme.spacing(1)
    }
  },
  dialogFooter: {
    justifyContent: 'flex-start',
    margin: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
    '& > span': {
      fontFamily: '"Nanum Gothic Coding", monospace'
    }
  },
  customFab: {
    width: 36,
    height: 36,
    display: 'flex',
    backgroundColor: '#3670a0',
    color: '#fed140',
    border: '2px solid rgba(55, 57, 63, 0.7)',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    '&:focus': {
      outline: 'none'
    },
    '& i': {
      fontSize: '1.5em'
    }
  },
  transparentButton: {
    backgroundColor: 'transparent',
    border: 'none',
    marginRight: -8
  }
}));

const YellowTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: yellow[700]
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: yellow[700]
    },
    '& .MuiOutlinedInput-root': {
      '& input': {
        color: '#fed140'
      },
      '& fieldset': {
        borderColor: '#fed140'
      },
      '&:hover fieldset': {
        borderColor: '#3670a0'
      },
      '&.Mui-focused fieldset': {
        borderColor: yellow[700]
      }
    }
  }
})(TextField);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Grow in timeout={1000} ref={ref} {...props} />;
});

function useInterval(callback, delay) {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

AdminLoginDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

export default function AdminLoginDialog({ open, onClose }) {
  const classes = useStyles();
  const history = useHistory();
  const [count, setCount] = useState(30);
  const [password, setPassword] = useState('');
  const [isRunning, setIsRunning] = useState(true);

  useInterval(
    () => {
      setCount(_count => _count - 1);
    },
    isRunning ? 1000 : null
  );

  useEffect(() => {
    if (open) {
      setIsRunning(true);
      setCount(30);
    }
  }, [open]);

  useEffect(() => {
    if (count <= 0) {
      setIsRunning(false);
      onClose();
    }
  }, [count]);

  const login = () => {
    if (ipcRenderer.sendSync('login', password)) {
      history.push('/admin/general');
    } else {
      setPassword('');
      onClose();
    }
  };

  return (
    <Dialog
      open={open}
      className={classes.dialog}
      TransitionComponent={Transition}
      keepMounted
      onClose={onClose}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogActions className={classes.dialogHeader}>
        <div>
          <button
            type="submit"
            onClick={onClose}
            className={clsx(classes.customFab, classes.transparentButton)}
          >
            <CloseIcon />
          </button>
        </div>
      </DialogActions>
      <DialogContent>
        <div className={classes.passwordForm}>
          <YellowTextField
            color="secondary"
            id="outlined-password-input"
            margin="dense"
            label="Password"
            className={classes.textField}
            type="password"
            autoComplete="current-password"
            variant="outlined"
            value={password}
            onChange={e => setPassword(e.target.value)}
            onKeyDown={e => e.keyCode === 13 && login()}
          />
          <Button variant="outlined" color="secondary" onClick={login}>
            Submit
          </Button>
        </div>
      </DialogContent>
      <DialogActions className={classes.dialogFooter}>
        <span>{count}</span>
        {'초 뒤에 자동으로 창이 닫힙니다.'}
      </DialogActions>
    </Dialog>
  );
}
