import React, { useEffect, useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { ipcRenderer } from 'electron';
import Snackbar from '@material-ui/core/Snackbar';
import AdminChangePasswordDialog from './AdminChangePasswordDialog';
import ButtonGroup from '@material-ui/core/ButtonGroup';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  item: {
    minHeight: '200px',
    display: 'flex',
    flexDirection: 'row',
    '& > div:first-child': {
      flex: '0 1 450px',
      padding: theme.spacing(3),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      '& h3': {
        marginTop: 0
      },
      borderRight: '1px solid rgba(255,255,255,0.3)'
    },
    '& > div:last-child': {
      flex: '1 1 auto',
      padding: theme.spacing(3),
      '& button': {
        minWidth: 160,
        height: 48
      }
    }
  },
  buttons: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    '& > button': {
      boxShadow: '0 3px 5px 2px rgba(255, 255, 255, 0.2)',
      marginBottom: theme.spacing(2)
    }
  },
  groupButton: {
    '& > button': {
      minWidth: '80px !important'
    }
  }
}));

const IOSSwitch = withStyles(theme => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1)
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: '#52d869',
        opacity: 1,
        border: 'none'
      }
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff'
    }
  },
  thumb: {
    width: 24,
    height: 24
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border'])
  },
  checked: {},
  focusVisible: {}
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked
      }}
      {...props}
    />
  );
});

const StyledButton = styled(Button)`
  background: linear-gradient(45deg, #fe6b8b 30%, #ff8e53 90%);
  border-radius: 3px;
  border: 0;
  color: white;
  height: 48px;
  padding: 0 30px;
  box-shadow: 0 3px 5px 2px rgba(255, 105, 135, 0.3);
`;

export default function AdminGeneral() {
  const classes = useStyles();
  const [kioskMode, setKioskMode] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');

  useEffect(() => {
    setKioskMode(ipcRenderer.sendSync('get-kiosk-mode'));
  }, []);

  const handleKioskModeChange = event => {
    ipcRenderer.send('set-kiosk-mode', event.target.checked);
    ipcRenderer.send('save-config', {
      name: 'config.kioskMode',
      value: event.target.checked
    });
    setKioskMode(event.target.checked);
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleDialogClose = message => {
    if (message) {
      setSnackbarMessage(message);
      setSnackbarOpen(true);
    }
    setDialogOpen(false);
  };

  const quitApp = () => {
    ipcRenderer.send('quit-app');
  };

  const quitDesktop = () => {
    ipcRenderer.send('quit-desktop');

    setSnackbarMessage('약 30초 뒤 시스템을 종료합니다.');
    setSnackbarOpen(true);
  };

  const registerStartUpApp = () => {
    ipcRenderer.send('set-open-at-login');

    setSnackbarMessage('시작 프로그램으로 등록되었습니다.');
    setSnackbarOpen(true);
  };

  const unregisterStartUpApp = () => {
    ipcRenderer.send('unset-open-at-login');

    setSnackbarMessage('시작 프로그램에서 해제되었습니다.');
    setSnackbarOpen(true);
  };

  return (
    <div className={classes.root}>
      <div className={classes.item}>
        <div>
          <h3>키오스크 모드</h3>
          <span>
            전체화면에서 프로그램을 사용할 수 있도록 변경하며, 키오스크 모드를
            해제하기 전에는 다른 윈도우와 상호작용 할 수 없습니다.
          </span>
        </div>
        <div>
          <IOSSwitch
            checked={kioskMode}
            onChange={handleKioskModeChange}
            name="kioskMode"
          />
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>비밀번호 변경</h3>
          <span>
            관리자 화면으로 접속하기 위한 비밀번호를 변경할 수 있습니다.
          </span>
        </div>
        <div>
          <Button
            variant="contained"
            color="default"
            onClick={() => setDialogOpen(true)}
          >
            변경하기
          </Button>
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>시작 프로그램 등록</h3>
          <span>
            키오스크 프로그램을 PC 시작 시 자동으로 시작되도록 합니다.
          </span>
        </div>
        <div className={classes.buttons}>
          <ButtonGroup
            className={classes.groupButton}
            variant="contained"
            aria-label="contained primary button group"
          >
            <Button onClick={registerStartUpApp}>등록</Button>
            <Button onClick={unregisterStartUpApp}>제거</Button>
          </ButtonGroup>
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>시스템 종료</h3>
          <span>키오스크 프로그램을 그것을 구동하는 PC와 함께 종료합니다.</span>
        </div>
        <div className={classes.buttons}>
          <Button variant="contained" color="default" onClick={quitApp}>
            키오스크만 종료
          </Button>
          <Button variant="contained" color="default" onClick={quitDesktop}>
            시스템 종료
          </Button>
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>설정 초기화</h3>
          <span>
            모든 설정을 초기화하고 기본 설정 값으로 되돌립니다. 등록한 컨텐츠와
            카테고리는 초기화되지 않습니다.
          </span>
        </div>
        <div>
          <StyledButton
            onClick={() => {
              ipcRenderer.sendSync('reset-config');
              setKioskMode(false);
              setSnackbarMessage('초기화가 완료되었습니다.');
              setSnackbarOpen(true);
            }}
          >
            초기화
          </StyledButton>
        </div>
      </div>

      <AdminChangePasswordDialog
        open={dialogOpen}
        onClose={handleDialogClose}
      />

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        open={snackbarOpen}
        autoHideDuration={3000}
        onClose={handleSnackbarClose}
        message={<span>{snackbarMessage}</span>}
      />
    </div>
  );
}
