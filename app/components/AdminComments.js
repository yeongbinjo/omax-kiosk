import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import { ipcRenderer } from 'electron';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import clsx from 'clsx';
import Snackbar from '@material-ui/core/Snackbar';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import Avatar1 from '../assets/images/avatar_1.png';
import Avatar2 from '../assets/images/avatar_2.png';
import Avatar3 from '../assets/images/avatar_3.png';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    '& .MuiListItemIcon-root': {
      minWidth: 45
    },
    '& .MuiListItemText-secondary': {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis'
    },
    '& .MuiListItemText-multiline': {
      marginRight: 16
    },
    '& .MuiListItemText-primary': {
      fontSize: '1.2rem',
      fontWeight: 500
    }
  },
  paper: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(2),
    '& .MuiList-root': {
      width: '100%'
    }
  },
  divider: {
    marginLeft: 16,
    marginRight: 16
  },
  iconButton: {
    padding: 10
  },
  red: {
    color: 'rgb(228,129,126)'
  },
  meta: {
    marginTop: 12,
    display: 'flex',
    '& > div': {
      display: 'flex',
      alignItems: 'center',
      marginRight: 12,
      fontWeight: 700,
      minWidth: 36
    },
    '& > div:nth-child(1)': {
      color: 'rgb(121,178,228)'
    },
    '& > div:nth-child(2)': {
      color: 'rgb(228,220,123)'
    },
    '& > div:nth-child(3)': {
      color: 'rgb(186,228,138)'
    },
    '& > div > .MuiSvgIcon-root': {
      fontSize: '1rem',
      marginRight: 5
    }
  },
  comments: {
    '& .MuiListItemText-root': {
      paddingBottom: 12,
      borderBottom: '1px solid rgba(255,255,255,0.2)'
    },
    '& .MuiListItemText-primary': {
      color: 'rgb(204,204,204)',
      fontSize: '0.875rem',
      '& .MuiTypography-body2': {
        color: 'rgb(255,255,255)',
        fontWeight: 700
      }
    },
    '& .MuiListItemText-secondary': {
      color: 'rgb(255,255,255)',
      fontSize: '1rem',
    }
  },
}));

export default function AdminComments() {
  const classes = useStyles();
  const [comments, setComments] = useState([]);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');

  useEffect(() => {
    setComments(ipcRenderer.sendSync('get-all-comments'));
  }, []);

  const deleteComment = comment => {
    if (
      ipcRenderer.sendSync('delete-comment', {
        itemId: comment.itemId,
        idx: comment.idx
      })
    ) {
      setSnackbarMessage('댓글이 삭제되었습니다.');
      setSnackbarOpen(true);
    }
    setComments(ipcRenderer.sendSync('get-all-comments'));
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const avatars = [Avatar1, Avatar2, Avatar3];

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <List aria-label="comments" className={classes.comments}>
          {comments.map(comment => (
            <ListItem key={comment.datetime} alignItems="flex-start">
              <ListItemAvatar>
                <Avatar alt={comment.nickname} src={avatars[comment.avatar]} />
              </ListItemAvatar>
              <ListItemText
                primary={
                  <>
                    <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                    >
                      {comment.nickname}
                    </Typography>
                    {` — ${moment(Number(comment.datetime)).fromNow()}`}
                  </>
                }
                secondary={comment.content}
              />
              <ListItemIcon onClick={() => deleteComment(comment)}>
                <IconButton
                  type="submit"
                  className={clsx(classes.iconButton, classes.red)}
                  aria-label="delete"
                >
                  <DeleteIcon />
                </IconButton>
              </ListItemIcon>
            </ListItem>
          ))}
        </List>
      </Paper>

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        open={snackbarOpen}
        autoHideDuration={3000}
        onClose={handleSnackbarClose}
        message={<span>{snackbarMessage}</span>}
      />
    </div>
  );
}
