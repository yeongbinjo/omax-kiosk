import React, { useEffect, useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link, useHistory, useParams } from 'react-router-dom';
import { ipcRenderer } from 'electron';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import Grow from '@material-ui/core/Grow';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import videojs from 'video.js';
import clsx from 'clsx';
import Snackbar from '@material-ui/core/Snackbar';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';
import IdleTimer from 'react-idle-timer';
import Keyboard from 'react-simple-keyboard';
import Hangul from 'hangul-js';
import Avatar1 from '../assets/images/avatar_1.png';
import Avatar2 from '../assets/images/avatar_2.png';
import Avatar3 from '../assets/images/avatar_3.png';

const layouts = {
  korean: {
    default: [
      '` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',
      '{tab} ㅂ ㅈ ㄷ ㄱ ㅅ ㅛ ㅕ ㅑ ㅐ ㅔ [ ] \\',
      "{한/영} ㅁ ㄴ ㅇ ㄹ ㅎ ㅗ ㅓ ㅏ ㅣ ; ' {enter}",
      '{shift} ㅋ ㅌ ㅊ ㅍ ㅠ ㅜ ㅡ , . / {shift}',
      '.com @ {space}'
    ],
    shift: [
      '~ ! @ # $ % ^ & * ( ) _ + {bksp}',
      '{tab} ㅃ ㅉ ㄸ ㄲ ㅆ ㅛ ㅕ ㅑ ㅒ ㅖ { } |',
      '{한/영} ㅁ ㄴ ㅇ ㄹ ㅎ ㅗ ㅓ ㅏ ㅣ : " {enter}',
      '{shift} ㅋ ㅌ ㅊ ㅍ ㅠ ㅜ ㅡ < > ? {shift}',
      '.com @ {space}'
    ]
  },
  english: {
    default: [
      '` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',
      '{tab} q w e r t y u i o p [ ] \\',
      "{한/영} a s d f g h j k l ; ' {enter}",
      '{shift} z x c v b n m , . / {shift}',
      '.com @ {space}'
    ],
    shift: [
      '~ ! @ # $ % ^ & * ( ) _ + {bksp}',
      '{tab} Q W E R T Y U I O P { } |',
      '{한/영} A S D F G H J K L : " {enter}',
      '{shift} Z X C V B N M < > ? {shift}',
      '.com @ {space}'
    ]
  }
};

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px`,
    flex: '1 1 auto'
  },
  videoContainer: {
    position: 'relative',
    padding: '0 !important',
    flex: '1 1 auto',
    display: 'flex',
    maxHeight: 600,
    '& video': {
      width: '100%',
      height: '100%'
    }
  },
  detail: {
    position: 'relative',
    backgroundColor: '#111111',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flex: '1 1 400px',
    padding: theme.spacing(3),
    paddingLeft: theme.spacing(6),
    paddingRight: theme.spacing(4),
    '& h3': {
      fontSize: '1.8rem',
      lineHeight: '2rem',
      fontWeight: 700
    },
    '& p': {
      fontSize: '1.2rem',
      fontWeight: 400,
      margin: `${theme.spacing(2)}px 0`,
      opacity: 0.7,
      lineHeight: '1.75rem',
      '& span': {
        fontWeight: 800
      }
    },
    '&:after': {
      content: '""',
      position: 'absolute',
      top: 0,
      right: -100,
      width: 100,
      height: 600,
      zIndex: 100,
      background: 'linear-gradient(to right, #111111, transparent)',
      pointerEvents: 'none'
    }
  },
  buttons: {
    '& button': {
      minWidth: 150,
      margin: theme.spacing(1),
      height: 60,
      fontSize: '1.5rem',
      letterSpacing: 2,
      '& i': {
        marginRight: theme.spacing(1)
      }
    },
    '& button:first-child': {
      marginLeft: 0,
      backgroundColor: '#fe1234',
      color: 'white'
    },
    '& button:last-child': {
      marginRight: 0
    },
    '& .MuiButton-outlined': {
      border: '2px solid rgba(255, 255, 255, 0.8)'
    }
  },
  video: {
    overflow: 'hidden',
    flex: '1 0 auto'
  },
  gridList: {
    '& .MuiGridListTile-tile': {
      position: 'relative',
      '& > a': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%'
      }
    },
    '& .MuiGridListTileBar-rootSubtitle': {
      height: 80
    },
    '& .MuiGridListTileBar-title': {
      fontWeight: 700,
      lineHeight: '1.3rem',
      maxHeight: '2.6rem',
      WebkitLineClamp: 2,
      whiteSpace: 'normal'
    },
    '& .MuiGridListTileBar-root': {
      backgroundColor: 'rgba(0,0,0,0.7)'
    },
    '& .MuiGridListTileBar-subtitle': {
      marginTop: '0.3rem'
    }
  },
  bookmark: {
    color: 'rgb(228,200,102)',
    borderColor: 'rgb(228,200,102) !important'
  },
  subtitle: {
    fontSize: '1.5rem !important',
    fontWeight: '400 !important',
    marginBottom: 8
  },
  black: {
    color: 'rgb(0,0,0)'
  },
  white: {
    color: 'rgb(255,255,255)'
  },
  noexpand: {
    flex: '0 1 auto',
    '& .header': {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '& .MuiIconButton-root': {
        color: 'rgba(255,255,255)',
        padding: 4,
        marginLeft: 4,
        marginBottom: 12
      }
    }
  },
  jua: {
    fontFamily: '"Jua", sans-serif',
    fontWeight: '100',
    fontSize: '1.1rem'
  },
  commentForm: {
    height: 'auto',
    display: 'block',
    visibility: 'visible',
    opacity: 1,
    transition: 'opacity 0.5s linear, max-height 0.5s',
    overflow: 'hidden',
    maxHeight: 180,
    paddingLeft: 16,
    paddingRight: 16,
    '& .header': {
      marginTop: 16,
      display: 'flex',
      flexDirection: 'row'
    },
    '& .header .MuiTextField-root': {
      width: 240,
      marginRight: 16
    },
    '& .header .MuiTextField-root .MuiInputLabel-root': {
      fontFamily: '"Jua", sans-serif',
      color: 'rgb(255,255,255)',
      '&.Mui-focused': {
        color: 'rgb(152,198,255)',
        borderColor: 'rgb(152,198,255)'
      }
    },
    '& .header .MuiInputBase-root': {
      fontFamily: '"Jua", sans-serif',
      color: 'rgb(255,255,255)',
      '& fieldset': {
        color: 'rgb(255,255,255)',
        borderColor: 'rgb(255,255,255)'
      },
      '&.Mui-focused fieldset': {
        color: 'rgb(152,198,255)',
        borderColor: 'rgb(152,198,255)'
      }
    },
    '& .body .MuiInputBase-root .MuiOutlinedInput-input': {
      fontFamily: '"Jua", sans-serif',
      color: 'rgb(255,255,255)',
      borderColor: 'rgb(255,255,255)'
    },
    '& .body': {
      marginBottom: 16,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center'
    },
    '& .body .MuiInputBase-root': {
      fontFamily: '"Jua", sans-serif',
      color: 'rgb(255,255,255)',
      '& fieldset': {
        color: 'rgb(255,255,255)',
        borderColor: 'rgb(255,255,255)'
      },
      '&.Mui-focused fieldset': {
        color: 'rgb(152,198,255)',
        borderColor: 'rgb(152,198,255)'
      }
    },
    '& .body .MuiTextField-root': {
      flex: '1 1 auto'
    },
    '& .body .MuiButtonBase-root': {
      marginLeft: 4,
      marginTop: 8,
      marginBottom: 4,
      height: 78,
      color: 'rgb(255,255,255)',
      borderColor: 'rgb(255,255,255)',
      '& .MuiButton-label': {
        fontSize: '1.2rem',
        fontFamily: '"Jua", sans-serif'
      },
      '&:hover': {
        color: 'rgb(152,198,255)',
        borderColor: 'rgb(152,198,255)',
        borderWidth: 2
      }
    }
  },
  inline: {
    display: 'inline'
  },
  comments: {
    '& .MuiListItemText-root': {
      paddingBottom: 12,
      borderBottom: '1px solid rgba(255,255,255,0.4)'
    },
    '& .MuiListItemText-primary': {
      color: 'rgb(175,175,175)',
      fontSize: '0.875rem',
      '& .MuiTypography-body2': {
        color: 'rgb(255,255,255)',
        fontWeight: 700
      }
    },
    '& .MuiListItemText-secondary': {
      color: 'rgb(255,255,255)',
      fontSize: '1rem'
    }
  },
  animationHidden: {
    maxHeight: 0,
    opacity: 0
  },
  hidden: {
    display: 'none !important'
  },
  showAllComments: {
    display: 'flex',
    alignItems: 'center',
    margin: '8px 0',
    opacity: 0.9,
    '& > span': {
      lineHeight: '1.2rem',
      fontSize: '1.2rem',
      fontFamily: '"Jua", sans-serif',
      marginLeft: 36,
      marginRight: 8
    },
    '& > svg': {
      width: 20
    },
    '&:hover': {
      cursor: 'pointer'
    }
  },
  keyboard: {
    position: 'fixed',
    bottom: 0,
    height: 230,
    width: 'calc(100vw - 260px)',
    zIndex: theme.zIndex.drawer + 5,
    transition: 'all .3s ease-in-out',
    '& .react-simple-keyboard': {
      color: 'black'
    }
  },
  keyboardHidden: {
    bottom: -230,
    height: 0
  }
}));

export default function Item() {
  const classes = useStyles();
  const { itemId } = useParams();
  const [items, setItems] = useState([]);
  const [item, setItem] = useState(null);
  const [bookmark, setBookmark] = useState(false);
  const [comments, setComments] = useState([]);
  const [isPlaying, setIsPlaying] = useState(true);

  const [showForm, setShowForm] = useState(false);
  const [showAllComments, setShowAllComments] = useState(false);
  const [nickname, setNickname] = useState('');
  const [password, setPassword] = useState('');
  const [content, setContent] = useState('');
  const [showKeyboard, setShowKeyboard] = useState(false);
  const [activeInput, setActiveInput] = useState(null);
  const [language, setLanguage] = useState('korean');
  const [lastButton, setLastButton] = useState(null);
  const [buttons, setButtons] = useState([]);

  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');

  const player = useRef(null);
  const videoNode = useRef(null);
  const idleTimer = useRef(null);
  const keyboard = useRef(null);

  const history = useHistory();

  useEffect(() => {
    setItems(ipcRenderer.sendSync('load-config', 'items'));
    setItem(ipcRenderer.sendSync('get-item', itemId));

    player.current = videojs(
      videoNode.current,
      {
        height: 600,
        autoplay: true,
        controls: true,
        sources: [
          {
            src: ipcRenderer.sendSync('get-item-path', {
              itemId,
              type: 1
            }),
            type: 'video/mp4'
          }
        ]
      },
      function onPlayerReady() {
        // console.log('onPlayerReady', this);
      }
    );

    player.current.on('pause', () => setIsPlaying(false));
    player.current.on('play', () => setIsPlaying(true));

    return () => {
      player.current.dispose();
      player.current.off('pause');
      player.current.off('play');
    };
  }, []);

  useEffect(() => {
    setItem(ipcRenderer.sendSync('get-item', itemId));
    setBookmark(ipcRenderer.sendSync('get-bookmark', itemId));
    setComments(ipcRenderer.sendSync('get-comments', itemId));
    ipcRenderer.send('add-view-count', itemId);

    if (player.current) {
      player.current.src({
        src: ipcRenderer.sendSync('get-item-path', {
          itemId,
          type: 1
        }),
        type: 'video/mp4'
      });
    }
  }, [itemId]);

  const toggleBookmark = () => {
    if (bookmark) {
      ipcRenderer.sendSync('unset-bookmark', itemId);
    } else {
      ipcRenderer.sendSync('set-bookmark', itemId);
    }
    setSnackbarMessage(
      bookmark ? '북마크를 해제하였습니다.' : '북마크를 등록하였습니다.'
    );
    setSnackbarOpen(true);
    setBookmark(!bookmark);
  };

  const toggleShowForm = () => {
    setShowForm(!showForm);
  };

  const togglePlay = () => {
    if (isPlaying) {
      player.current.pause();
    } else {
      player.current.play();
    }
    // setIsPlaying(!isPlaying);
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const addComment = () => {
    if (!nickname) {
      setSnackbarMessage('닉네임을 입력하세요.');
      setSnackbarOpen(true);
      return;
    }
    if (!password) {
      setSnackbarMessage('비밀번호를 입력하세요.');
      setSnackbarOpen(true);
      return;
    }
    if (!content) {
      setSnackbarMessage('내용을 입력하세요.');
      setSnackbarOpen(true);
      return;
    }

    ipcRenderer.sendSync('add-comment', {
      itemId,
      comment: {
        nickname,
        password,
        content,
        avatar: Math.floor(Math.random() * 3),
        datetime: moment().format('x'),
        modified: false
      }
    });
    setContent('');
    setComments(ipcRenderer.sendSync('get-comments', itemId));
    setSnackbarMessage('댓글이 등록되었습니다.');
    setSnackbarOpen(true);
    setShowForm(false);
  };

  const onShowAllComments = () => {
    setShowAllComments(true);
  };

  const onIdle = () => {
    if (player.current && !player.current.paused()) {
      return;
    }
    history.push('/');
  };

  const onInputChange = e => {
    keyboard.current.setInput(e.target.value ? e.target.value : '');
    setButtons(Hangul.disassemble(e.target.value));
    if (e.target.name === 'nickname') {
      setNickname(e.target.value);
    } else if (e.target.name === 'password') {
      setPassword(e.target.value);
    } else if (e.target.name === 'content') {
      setContent(e.target.value);
    }
  };

  const onFocus = e => {
    keyboard.current.setInput(e.target.value ? e.target.value : '');
    setButtons(Hangul.disassemble(e.target.value));
    setActiveInput(e.target.name);
    setShowKeyboard(true);
  };

  const onBlur = e => {
    if (e.relatedTarget) {
      if (e.relatedTarget.id === 'keyboard') {
        e.preventDefault();
        e.stopPropagation();
      } else if (
        !['content', 'password', 'nickname'].includes(e.relatedTarget.id)
      ) {
        if (!['{shift}', '{한/영}'].includes(lastButton)) {
          setShowKeyboard(false);
          setActiveInput(null);
        }
      }
    } else if (!['{shift}', '{한/영}'].includes(lastButton)) {
      setShowKeyboard(false);
      setActiveInput(null);
    }
  };

  const onChange = () => {
    const assembledInput = Hangul.assemble(buttons);

    if (activeInput === 'nickname') {
      setNickname(assembledInput);
    } else if (activeInput === 'password') {
      setPassword(assembledInput);
    } else if (activeInput === 'content') {
      setContent(assembledInput);
    } else {
      // do nothing
    }
  };

  const onKeyPress = button => {
    if (
      !['{shift}', '{한/영}', '{enter}', '{bksp}', '{space}', '{tab}'].includes(
        button
      )
    ) {
      buttons.push(button);
    }
    if (button === '{bksp}') {
      buttons.pop();
    }
    if (button === '{space}') {
      buttons.push(' ');
    }
    if (button === '{tab}') {
      buttons.push('  ');
    }

    setLastButton(button);
    setButtons(buttons);
    if (button === '{shift}' || button === '{lock}') handleShift();
    if (button === '{한/영}') handleLanguageButton();
  };

  const handleShift = () => {
    const currentLayout = keyboard.current.options.layoutName;
    const shiftToggle = currentLayout === 'default' ? 'shift' : 'default';

    keyboard.current.setOptions({
      layoutName: shiftToggle
    });
  };

  const handleLanguageButton = () => {
    const toggle = language === 'korean' ? 'english' : 'korean';
    setLanguage(toggle);
    keyboard.current.setOptions({
      language: toggle,
      layout: language[toggle]
    });
  };

  const relatedItems = items.filter(
    v => item.category === v.category && item.id !== v.id
  );

  const avatars = [Avatar1, Avatar2, Avatar3];
  const shownComments = showAllComments ? comments : comments.slice(0, 5);

  return (
    <>
      <Container maxWidth="xl" className={classes.videoContainer}>
        <div className={classes.detail}>
          <Typography component="h3" variant="h1" color="inherit">
            {item ? item.title : ''}
          </Typography>
          <Typography component="p" variant="body1" color="inherit">
            {item ? item.description : ''}
          </Typography>
          <div className={classes.buttons}>
            <Button
              variant="contained"
              color="default"
              onClick={() => togglePlay()}
            >
              {isPlaying ? (
                <>
                  <i className="fas fa-pause" /> 정지
                </>
              ) : (
                <>
                  <i className="fas fa-play" /> 재생
                </>
              )}
            </Button>
            <Button
              variant="outlined"
              color="default"
              className={clsx(bookmark && classes.bookmark)}
              onClick={toggleBookmark}
            >
              <i className="fas fa-star" /> 즐겨찾기
            </Button>
          </div>
          <Typography
            component="p"
            variant="body1"
            color="inherit"
            style={{ marginTop: 32 }}
          >
            <span>카테고리:</span>{' '}
            {item
              ? ipcRenderer.sendSync('get-category-name', item.category)
              : ''}
          </Typography>
        </div>
        <div data-vjs-player className={classes.video}>
          <video ref={videoNode} className="video-js" />
        </div>
      </Container>

      <Container
        maxWidth="xl"
        className={clsx(classes.container, classes.noexpand)}
      >
        <div className="header">
          <Typography
            varaint={'body1'}
            className={clsx(classes.subtitle, classes.white, classes.jua)}
          >
            {`댓글 ${comments ? comments.length : 0}개`}
          </Typography>
          <IconButton
            className={clsx(classes.iconButton)}
            aria-label="show"
            onClick={toggleShowForm}
          >
            <AddCircleIcon />
          </IconButton>
        </div>
        <div
          className={clsx(
            classes.commentForm,
            !showForm && classes.animationHidden
          )}
        >
          <div className="header">
            <TextField
              label="닉네임"
              variant="outlined"
              margin="dense"
              type="text"
              id="nickname"
              name="nickname"
              value={nickname}
              onChange={onInputChange}
              onFocus={onFocus}
              onBlur={onBlur}
            />
            <TextField
              label="비밀번호"
              variant="outlined"
              margin="dense"
              type="password"
              id="password"
              name="password"
              value={password}
              onChange={onInputChange}
              onFocus={onFocus}
              onBlur={onBlur}
            />
          </div>
          <div className="body">
            <TextField
              placeholder="건전한 댓글 부탁드립니다!"
              variant="outlined"
              margin="dense"
              multiline
              rows="3"
              id="content"
              name="content"
              value={content}
              onChange={onInputChange}
              onFocus={onFocus}
              onBlur={onBlur}
            />
            <Button
              variant="outlined"
              className="comment-button"
              onClick={addComment}
            >
              등록하기
            </Button>
          </div>
        </div>
        {!comments || comments.length === 0 ? (
          <Typography
            variant={'body2'}
            className={clsx(classes.white, classes.jua)}
          >
            가장 먼저 댓글을 남겨주세요!
          </Typography>
        ) : (
          <List className={classes.comments}>
            {shownComments.map((comment, index) => (
              <ListItem
                key={comment.datetime}
                alignItems="flex-start"
                style={{
                  opacity:
                    showAllComments || index < 3 ? 1 : 1 - (index - 2) * 0.3
                }}
              >
                <ListItemAvatar>
                  <Avatar
                    alt={comment.nickname}
                    src={avatars[comment.avatar]}
                  />
                </ListItemAvatar>
                <ListItemText
                  primary={
                    <>
                      <Typography
                        component="span"
                        variant="body2"
                        className={classes.inline}
                      >
                        {comment.nickname}
                      </Typography>
                      {` — ${moment(Number(comment.datetime)).fromNow()}`}
                    </>
                  }
                  secondary={comment.content}
                />
              </ListItem>
            ))}
          </List>
        )}
        <div
          className={clsx(
            classes.showAllComments,
            showAllComments || comments.length <= 5 ? classes.hidden : null
          )}
          onClick={onShowAllComments}
        >
          <span>나머지 {comments.length - 5}개 댓글 보기</span>
          <PlayArrowIcon />
          <PlayArrowIcon />
          <PlayArrowIcon />
        </div>
      </Container>

      <Container maxWidth="xl" className={classes.container}>
        <Typography
          varaint={'body1'}
          className={clsx(classes.subtitle, classes.white, classes.jua)}
        >
          관련 영상
        </Typography>
        {!relatedItems || relatedItems.length === 0 ? (
          <Typography
            variant={'body2'}
            className={clsx(classes.white, classes.jua)}
          >
            관련된 영상이 없습니다.
          </Typography>
        ) : null}
        <GridList
          cellHeight={180}
          className={classes.gridList}
          cols={5}
          spacing={16}
        >
          {relatedItems.map((v, index) => (
            <Grow
              in
              timeout={{
                enter: index * 200
              }}
              key={v.title}
            >
              <GridListTile>
                <Link to={`/item/${v.id}`}>
                  <img
                    src={ipcRenderer.sendSync('get-item-path', {
                      itemId: v.id,
                      type: 2
                    })}
                    style={{ width: '100%', height: '100%' }}
                    alt={v.title}
                  />
                  <GridListTileBar
                    title={v.title}
                    subtitle={<span>{v.description}</span>}
                    actionIcon={
                      <IconButton
                        aria-label={`info about ${v.title}`}
                        className={classes.icon}
                      >
                        <InfoIcon />
                      </IconButton>
                    }
                  />
                </Link>
              </GridListTile>
            </Grow>
          ))}
        </GridList>
      </Container>

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        open={snackbarOpen}
        autoHideDuration={3000}
        onClose={handleSnackbarClose}
        message={<span>{snackbarMessage}</span>}
      />

      <IdleTimer
        ref={idleTimer}
        element={document}
        onIdle={onIdle}
        debounce={250}
        timeout={1000 * 60 * 5}
      />

      <div
        tabIndex="0"
        id="keyboard"
        className={clsx(
          classes.keyboard,
          !showKeyboard && classes.keyboardHidden
        )}
        onBlur={onBlur}
      >
        <Keyboard
          keyboardRef={r => {
            keyboard.current = r;
          }}
          onChange={onChange}
          onKeyPress={onKeyPress}
          layout={layouts[language]}
        />
      </div>
    </>
  );
}
