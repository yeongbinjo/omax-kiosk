import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Grow from '@material-ui/core/Grow';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { yellow } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  button: {
    marginLeft: theme.spacing(1),
    marginTop: 8,
    marginBottom: 4,
    height: 40,
  }
}));

const YellowTextField = withStyles({
  root: {
    '& label.Mui-focused': {
      color: yellow[700]
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: yellow[700]
    },
    '& .MuiOutlinedInput-root': {
      '& input': {
        color: '#fed140'
      },
      '& fieldset': {
        borderColor: '#fed140'
      },
      '&:hover fieldset': {
        borderColor: '#3670a0'
      },
      '&.Mui-focused fieldset': {
        borderColor: yellow[700]
      }
    }
  }
})(TextField);

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Grow in timeout={1000} ref={ref} {...props} />;
});

AdminEditCategoryDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired
};

export default function AdminEditCategoryDialog({ open, text, onClose }) {
  const classes = useStyles();
  const [state, setState] = useState({
    text
  });

  const handleChange = e => {
    setState({ ...state, text: e.target.value });
  };

  return (
    <Dialog
      open={open}
      className={classes.dialog}
      TransitionComponent={Transition}
      onClose={() => onClose('')}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogContent>
        <div className={classes.passwordForm}>
          <YellowTextField
            color="secondary"
            id="outlined-text-input"
            margin="dense"
            label="카테고리 이름"
            value={state.text}
            onChange={handleChange}
            type="text"
            variant="outlined"
          />
          <Button
            variant="outlined"
            color="secondary"
            className={classes.button}
            onClick={() => onClose(state.text)}
          >
            수정
          </Button>
        </div>
      </DialogContent>
    </Dialog>
  );
}
