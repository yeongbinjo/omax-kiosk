import React, { useEffect, useState, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link, useParams } from 'react-router-dom';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Grow from '@material-ui/core/Grow';
import Container from '@material-ui/core/Container';
import { ipcRenderer } from 'electron';
import IdleTimer from 'react-idle-timer';
import IdleVideo from './IdleVideo';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative',
    padding: theme.spacing(4),
    flex: '1 1 auto'
  },
  gridList: {
    '& .MuiGridListTile-tile': {
      position: 'relative',
      '& > a': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%'
      }
    },
    '& .MuiGridListTileBar-rootSubtitle': {
      height: 80
    },
    '& .MuiGridListTileBar-title': {
      fontWeight: 700,
      lineHeight: '1.3rem',
      maxHeight: '2.6rem',
      WebkitLineClamp: 2,
      whiteSpace: 'normal'
    },
    '& .MuiGridListTileBar-root': {
      backgroundColor: 'rgba(0,0,0,0.8)'
    },
    '& .MuiGridListTileBar-subtitle': {
      marginTop: '0.3rem',
      color: 'rgba(200,200,200,1)'
    }
  }
}));

export default function Home() {
  const classes = useStyles();
  const [items, setItems] = useState([]);
  const [idleVideoOpen, setIdleVideoOpen] = useState(false);
  const { type, category } = useParams();
  const idleTimer = useRef(null);

  useEffect(() => {
    setItems(ipcRenderer.sendSync('load-config', 'items'));

    // 홈 화면에서 5분간 유휴 상태면 대기 영상이 자동 재생된다
  }, []);

  let filteredItems = items.filter(
    item => !category || category === item.category
  );
  if (type === 'rank') {
    filteredItems.sort((l, r) => {
      const lC = ipcRenderer.sendSync('get-view-count', l.id);
      const rC = ipcRenderer.sendSync('get-view-count', r.id);
      if (lC < rC) return 1;
      if (lC > rC) return -1;
      return 0;
    });
  } else if (type === 'bookmark') {
    filteredItems = filteredItems.filter(item =>
      ipcRenderer.sendSync('get-bookmark', item.id)
    );
  }

  const onIdleVideoClose = () => {
    setIdleVideoOpen(false);
  };

  const onIdle = () => {
    setIdleVideoOpen(true);
  };

  return (
    <Container maxWidth="xl" className={classes.container}>
      <GridList
        cellHeight={180}
        className={classes.gridList}
        cols={5}
        spacing={16}
      >
        {filteredItems.map((item, index) => (
          <Grow
            in
            timeout={{
              enter: index * 200,
              exit: 500
            }}
            mountOnEnter
            unmountOnExit
            key={item.id}
          >
            <GridListTile>
              <Link to={`/item/${item.id}`}>
                <img
                  src={ipcRenderer.sendSync('get-item-path', {
                    itemId: item.id,
                    type: 2
                  })}
                  style={{ width: '100%', height: '100%' }}
                  alt={item.title}
                />
                <GridListTileBar
                  title={item.title}
                  subtitle={<span>{item.description}</span>}
                />
              </Link>
            </GridListTile>
          </Grow>
        ))}
      </GridList>

      <IdleTimer
        ref={idleTimer}
        element={document}
        onIdle={onIdle}
        debounce={250}
        timeout={1000 * 60 * 5}
        // timeout={1000 * 5}
      />
      <IdleVideo open={idleVideoOpen} onClose={onIdleVideoClose} />
    </Container>
  );
}
