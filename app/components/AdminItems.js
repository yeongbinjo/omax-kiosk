import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import StarIcon from '@material-ui/icons/Star';
import CommentIcon from '@material-ui/icons/Comment';
import { ipcRenderer } from 'electron';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import clsx from 'clsx';
import Snackbar from '@material-ui/core/Snackbar';
import AdminEditItemDialog from './AdminEditItemDialog';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    '& .MuiListItemIcon-root': {
      minWidth: 45
    },
    '& .MuiListItemAvatar-root': {
      minWidth: 186
    },
    '& .MuiAvatar-root': {
      borderRadius: 0,
      height: 100,
      width: 170
    },
    '& .MuiListItemText-secondary': {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis'
    },
    '& .MuiListItemText-multiline': {
      marginRight: 16
    },
    '& .MuiListItemText-primary': {
      fontSize: '1.2rem',
      fontWeight: 500
    }
  },
  paper: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(2),
    '& .MuiList-root': {
      width: '100%'
    }
  },
  divider: {
    marginLeft: 16,
    marginRight: 16
  },
  iconButton: {
    padding: 10
  },
  red: {
    color: 'rgb(228,129,126)'
  },
  meta: {
    marginTop: 12,
    display: 'flex',
    '& > div': {
      display: 'flex',
      alignItems: 'center',
      marginRight: 12,
      fontWeight: 700,
      minWidth: 36
    },
    '& > div:nth-child(1)': {
      color: 'rgb(121,178,228)'
    },
    '& > div:nth-child(2)': {
      color: 'rgb(228,220,123)'
    },
    '& > div:nth-child(3)': {
      color: 'rgb(186,228,138)'
    },
    '& > div > .MuiSvgIcon-root': {
      fontSize: '1rem',
      marginRight: 5
    }
  }
}));

export default function AdminItems() {
  const classes = useStyles();
  const [items, setItems] = useState([]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const [item, setItem] = useState(null);

  useEffect(() => {
    setItems(ipcRenderer.sendSync('load-config', 'items'));
  }, []);

  const deleteItem = itemId => {
    if (ipcRenderer.sendSync('delete-item', itemId)) {
      setSnackbarMessage('영상이 삭제되었습니다.');
      setSnackbarOpen(true);
    }
    setItems(ipcRenderer.sendSync('load-config', 'items'));
  };

  const editItem = target => {
    setItem(target);
    setDialogOpen(true);
  };

  const handleDialogClose = message => {
    if (message) {
      setItems(ipcRenderer.sendSync('load-config', 'items'));
      setSnackbarMessage(message);
      setSnackbarOpen(true);
    }
    setDialogOpen(false);
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <List aria-label="items">
          {items.map(v => (
            <div key={v.id}>
              <ListItem>
                <ListItemAvatar>
                  <Avatar
                    alt={v.title}
                    src={`${ipcRenderer.sendSync('get-item-path', {
                      itemId: v.id,
                      type: 2
                    })}?c=${Math.floor(Date.now() / 1000)}`}
                  />
                </ListItemAvatar>
                <ListItemText
                  primary={v.title}
                  secondary={
                    <>
                      <span>{v.description}</span>
                      <div className={classes.meta}>
                        <div>
                          <VisibilityIcon />
                          {ipcRenderer.sendSync('get-view-count', v.id)}
                        </div>
                        <div>
                          <StarIcon />
                          {ipcRenderer.sendSync('get-bookmark', v.id) ? 1 : 0}
                        </div>
                        <div>
                          <CommentIcon />
                          {(() => {
                            const comments = ipcRenderer.sendSync(
                              'get-comments',
                              v.id
                            );
                            return comments ? comments.length : 0;
                          })()}
                        </div>
                      </div>
                    </>
                  }
                />
                <ListItemIcon onClick={() => editItem(v)}>
                  <IconButton
                    type="submit"
                    className={classes.iconButton}
                    aria-label="edit"
                  >
                    <EditIcon />
                  </IconButton>
                </ListItemIcon>
                <ListItemIcon onClick={() => deleteItem(v.id)}>
                  <IconButton
                    type="submit"
                    className={clsx(classes.iconButton, classes.red)}
                    aria-label="delete"
                  >
                    <DeleteIcon />
                  </IconButton>
                </ListItemIcon>
              </ListItem>
              <Divider className={classes.divider} component="li" />
            </div>
          ))}
        </List>
      </Paper>

      <AdminEditItemDialog
        open={dialogOpen}
        target={item}
        onClose={handleDialogClose}
      />

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
        message={<span>{snackbarMessage}</span>}
      />
    </div>
  );
}
