import React, { useState, useRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { FilePond } from 'react-filepond';
import { ipcRenderer } from 'electron';
import Snackbar from '@material-ui/core/Snackbar';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import YouTubeIcon from '@material-ui/icons/YouTube';
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';
import InputBase from '@material-ui/core/InputBase';
import clsx from 'clsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import { getId } from '../utils';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  item: {
    minHeight: '150px',
    display: 'flex',
    flexDirection: 'row',
    '& > div:first-child': {
      flex: '0 1 450px',
      padding: theme.spacing(3),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      '& h3': {
        marginTop: 0
      },
      borderRight: '1px solid rgba(255,255,255,0.3)'
    },
    '& > div:last-child': {
      flex: '1 1 auto',
      padding: theme.spacing(3),
      '& label.Mui-focused': {
        color: 'white'
      },
      '& button:not(.filepond--file-action-button)': {
        minWidth: 160,
        height: 48
      }
    }
  },
  formControl: {
    minWidth: 120
  },
  paper: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    marginBottom: theme.spacing(2),
    '& .MuiList-root': {
      width: '100%'
    },
    '& .MuiIconButton-root': {
      minWidth: '48px !important'
    },
    '& .MuiInputBase-root input': {
      fontFamily: "'NanumSquare', sans-serif"
    }
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  hidden: {
    display: 'none'
  }
}));

const StyledButton = styled(Button)`
  background: linear-gradient(45deg, #6b98fe 30%, #53ffcb 90%);
  border-radius: 3px;
  border: 0;
  color: black;
  height: 48px;
  padding: 0 30px;
  box-shadow: 0 3px 5px 2px rgba(255, 255, 255, 0.3);
`;

export default function AdminGeneral() {
  const classes = useStyles();
  const [state, setState] = useState({
    title: '',
    description: '',
    category: '',
    categories: []
  });
  const [youTubeUrl, setYouTubeUrl] = useState('');
  const [item, setItem] = useState('');
  const [thumbnail, setThumbnail] = useState('');
  const [disabled, setDisabled] = useState(false);
  const pond1 = useRef(null);
  const pond2 = useRef(null);

  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');

  useEffect(() => {
    setState({
      ...state,
      categories: ipcRenderer.sendSync('load-config', 'categories')
    });

    ipcRenderer.on('add-item-reply', (event, arg) => {
      console.log(arg);
      setDisabled(false);

      if (arg) {
        setSnackbarMessage('영상이 성공적으로 등록되었습니다.');
        setSnackbarOpen(true);
      } else {
        setSnackbarMessage('영상 등록에 실패하였습니다.');
        setSnackbarOpen(true);
      }

      setState({
        title: '',
        description: '',
        category: '',
        categories: ipcRenderer.sendSync('load-config', 'categories')
      });
      pond1.current.removeFiles();
      pond2.current.removeFiles();
      setItem(null);
      setThumbnail(null);
    });

    ipcRenderer.on('get-youtube-reply', (event, arg) => {
      console.log(arg);
      setDisabled(false);

      if (arg && arg.success) {
        setSnackbarMessage('영상 정보를 성공적으로 가져왔습니다.');
        setYouTubeUrl('');
        setSnackbarOpen(true);
      } else {
        setSnackbarMessage('영상 정보를 가져오는데 실패하였습니다.');
        setSnackbarOpen(true);
      }

      setState({
        title: arg.title,
        description: arg.description,
        category: '',
        categories: ipcRenderer.sendSync('load-config', 'categories')
      });
      pond1.current.addFile(arg.video);
      pond2.current.addFile(arg.thumbnail);
      setItem(arg.video);
      setThumbnail(arg.thumbnail);
    });

    return () => {
      ipcRenderer.removeAllListeners('add-item-reply');
      ipcRenderer.removeAllListeners('get-youtube-reply');
    };
  }, []);

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.value });
  };

  const addItem = () => {
    setDisabled(true);
    ipcRenderer.send('add-item', {
      id: getId(16),
      title: state.title,
      description: state.description,
      category: state.category,
      item: item.path || item,
      thumbnail: thumbnail.path || thumbnail
    });
  };

  const getInfo = () => {
    setDisabled(true);
    ipcRenderer.send('get-youtube', {
      url: youTubeUrl
    });
  };

  const onUpdateFiles = files => {
    if (files.length > 0) {
      if (files[0].file && files[0].file.path) {
        setItem(files[0].file.path);
      }
    }
  };

  return (
    <div className={classes.root}>
      <div className={classes.item}>
        <div>
          <h3>YouTube에서 가져오기</h3>
          <span>
            등록할 YouTube 영상의 주소를 입력해주세요. 영상 다운로드에 다소
            시간이 걸릴 수 있습니다.
          </span>
        </div>
        <div>
          <Paper className={classes.paper}>
            <IconButton className={classes.iconButton} aria-label="menu">
              <YouTubeIcon style={{ color: 'red' }} />
            </IconButton>
            <InputBase
              className={classes.input}
              placeholder="유튜브 영상 URL"
              inputProps={{ 'aria-label': 'get information' }}
              disabled={disabled}
              value={youTubeUrl}
              onChange={e => setYouTubeUrl(e.target.value)}
            />
            <IconButton
              type="submit"
              className={clsx(classes.iconButton, disabled && classes.hidden)}
              aria-label="add"
              onClick={getInfo}
            >
              <KeyboardReturnIcon />
            </IconButton>
            <CircularProgress
              color="secondary"
              size={36}
              thickness={4}
              style={{ marginRight: 12 }}
              className={clsx(!disabled && classes.hidden)}
            />
          </Paper>
        </div>
      </div>

      <div className={classes.item}>
        <div>
          <h3>영상 제목</h3>
          <span>영상의 제목을 입력하세요.</span>
        </div>
        <div>
          <TextField
            id="filled-full-width"
            label="영상 제목"
            style={{ marginTop: -8 }}
            placeholder="영상 제목"
            fullWidth
            margin="normal"
            variant="filled"
            disabled={disabled}
            value={state.title}
            name="title"
            onChange={handleChange}
          />
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>영상 설명</h3>
          <span>영상의 설명을 입력하세요.</span>
        </div>
        <div>
          <TextField
            id="filled-full-width"
            label="영상 설명"
            style={{ marginTop: -8 }}
            placeholder="영상 설명"
            multiline
            rows="8"
            fullWidth
            margin="normal"
            variant="filled"
            disabled={disabled}
            value={state.description}
            name="description"
            onChange={handleChange}
          />
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>영상 카테고리</h3>
          <span>
            영상의 카테고리를 입력하세요. 카테고리에 대한 설정은{' '}
            <em>카테고리 관리</em>에서 가능합니다.
          </span>
        </div>
        <div>
          <FormControl
            variant="filled"
            className={classes.formControl}
            style={{ marginTop: -8 }}
          >
            <InputLabel id="demo-simple-select-filled-label">
              카테고리
            </InputLabel>
            <Select
              labelId="demo-simple-select-filled-label"
              id="demo-simple-select-filled"
              disabled={disabled}
              value={state.category}
              onChange={handleChange}
              name="category"
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {state.categories.map(c => (
                <MenuItem key={c.id} value={c.id}>
                  {c.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>영상 파일</h3>
          <span>
            등록할 영상 파일을 우측 영역에 올려주세요. MP4 포맷만 현재
            지원합니다.
          </span>
        </div>
        <div>
          <FilePond
            ref={pond1}
            allowMultiple={false}
            maxFiles={1}
            acceptedFileTypes={['video/*']}
            onupdatefiles={onUpdateFiles}
          />
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>영상 섬네일</h3>
          <span>영상 목록에 노출될 섬네일 이미지 파일을 등록해주세요.</span>
        </div>
        <div>
          <FilePond
            ref={pond2}
            allowMultiple={false}
            maxFiles={1}
            acceptedFileTypes={['image/*']}
            onupdatefiles={onUpdateFiles}
          />
        </div>
      </div>
      <div className={classes.item}>
        <div>
          <h3>영상 저장 및 등록</h3>
          <span>
            영상을 지정된 위치에 은닉하고 암호화하여 저장합니다. 등록된 영상은
            바로 게시됩니다.
          </span>
        </div>
        <div>
          <StyledButton disabled={disabled} onClick={addItem}>
            등록
          </StyledButton>
        </div>
      </div>

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        open={snackbarOpen}
        autoHideDuration={3000}
        onClose={handleSnackbarClose}
        message={<span>{snackbarMessage}</span>}
      />
    </div>
  );
}
