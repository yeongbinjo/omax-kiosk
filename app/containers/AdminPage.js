import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Admin from '../components/Admin';
import * as AdminActions from '../actions/admin';

function mapStateToProps(state) {
  return {
    counter: state.counter
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(AdminActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
