import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { Link, useLocation } from 'react-router-dom';
import { Switch, Route } from 'react-router';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import { ipcRenderer } from 'electron';
import AdminLoginDialog from '../components/AdminLoginDialog';
import HiddenBar from '../components/HiddenBar';
import Logo from '../assets/images/omax_logo.png';
import Home from '../components/Home';
import Item from '../components/Item';

const drawerWidth = 260;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    backgroundColor: '#58595B'
  },
  toolbar: {
    minHeight: 100,
    padding: '16px',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
    '& a': {
      color: '#fff',
      textDecoration: 'none',
      textAlign: 'center',
      margin: '0 32px',
    },
    '& h1': {
      margin: 0
    }
  },
  toolbarLogo: {
    minHeight: 100,
    backgroundColor: '#302e31',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    padding: '0 8px',
    '-webkit-app-region': 'drag',
    '& img': {
      paddingTop: 16,
      paddingBottom: 16
    }
  },
  appBar: {
    zIndex: theme.zIndex.drawer - 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    boxShadow: 'none'
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: 'none'
  },
  titleMenu: {
    marginRight: theme.spacing(5),
    fontFamily: "'NanumSquare', sans-serif",
    fontSize: '1.2rem',
    fontWeight: 900,
    color: '#ffffff'
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth - 40,
    marginLeft: 40,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    }),
    border: 'none',
    backgroundColor: '#464648',
    boxShadow:
      '0px 8px 10px 5px rgba(0,0,0,0.4), ' +
      '0px 16px 24px 2px rgba(0,0,0,0.28), ' +
      '0px 6px 30px 5px rgba(0,0,0,0.24)'
  },
  appBarSpacer: {
    minHeight: 100,
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start'
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column'
  },
  sideMenu: {
    paddingTop: 32,
    paddingBottom: 32,
    '& li': {
      padding: '0 !important',
      '& a': {
        width: '100%'
      }
    }
  },
  active: {
    color: 'rgb(80,149,246)',
    textShadow: '2px 2px 7px rgba(144,181,221,0.3)'
  },
  activeDrawerMenu: {
    backgroundColor: `rgba(1,113,187,1) !important`
  },
  drawerMenuIcon: {
    position: 'absolute',
    right: 20,
    fontSize: '1.9rem'
  },
  animatedIcon: {
    opacity: 0.8,
    transition: '.25s opacity'
  },
  hidden: {
    opacity: 0
  },
  customFab: {
    width: 48,
    height: 48,
    display: 'flex',
    backgroundColor: 'transparent',
    color: '#fed140',
    border: '3px solid',
    borderColor: 'rgba(54,112,160,0.7)',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
    '&:focus': {
      outline: 'none'
    },
    '& i': {
      fontSize: '2em'
    }
  },
  quitButton: {
    position: 'absolute',
    right: 20,
    top: 26
  },
  drawer: {
    '&::before': {
      content: '""',
      position: 'absolute',
      display: 'block',
      top: 0,
      left: 0,
      height: 100,
      width: 40,
      backgroundColor: '#383639'
    }
  }
}));

const StyledButton = withStyles({
  root: {
    borderRadius: 0,
    width: '100%',
    height: 72,
    position: 'relative',
    color: 'unset',
    boxShadow: 'none',
    backgroundColor: 'transparent',
    '&:hover': {
      backgroundColor: `rgba(211, 241, 255, 0.1)`,
      boxShadow: 'none'
    },
    '&:active': {
      backgroundColor: `rgba(1,113,187,1)`
    },
    '& .MuiTouchRipple-child': {
      backgroundColor: `rgba(255, 255, 255, 0.3)`
    }
  },
  label: {
    fontFamily: "'NanumSquare', sans-serif",
    fontWeight: 900,
    fontSize: '1.6rem',
    color: 'white'
  }
})(Button);

export default function RootPage() {
  const classes = useStyles();
  const location = useLocation();
  const [open, setOpen] = useState(false);
  const [categories, setCategories] = useState([]);

  const handleClose = () => {
    setOpen(false);
  };

  const openAdminDialog = () => {
    setOpen(true);
  };

  useEffect(() => {
    setCategories(ipcRenderer.sendSync('load-config', 'categories'));
  }, []);

  let type = '';
  if (location.pathname.startsWith('/rank')) {
    type = 'rank';
  } else if (location.pathname.startsWith('/bookmark')) {
    type = 'bookmark';
  }
  const tokens = location.pathname.split('/');
  const category = tokens[tokens.length - 1];

  const quitDesktop = () => {
    ipcRenderer.send('quit-desktop');
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="absolute"
        className={clsx(classes.appBar, classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <Link to="/">
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              className={clsx(classes.titleMenu, type === '' && classes.active)}
            >
              홈
            </Typography>
          </Link>
          <Link to="/rank">
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              className={clsx(
                classes.titleMenu,
                type === 'rank' && classes.active
              )}
            >
              인기 콘텐츠
            </Typography>
          </Link>
          <Link to="/bookmark">
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              className={clsx(
                classes.titleMenu,
                type === 'bookmark' && classes.active
              )}
            >
              북마크
            </Typography>
          </Link>

          <button
            type="submit"
            onClick={() => quitDesktop()}
            className={clsx(classes.customFab, classes.quitButton)}
          >
            <i className="fa fa-power-off fa-2x" />
          </button>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper)
        }}
        className={classes.drawer}
      >
        <div className={classes.toolbarLogo}>
          <img src={Logo} alt="omax logo" />
        </div>
        <List className={classes.sideMenu}>
          {categories.map(c => (
            <ListItem key={c.id}>
              <Link to={`${type ? `/${type}` : ''}/category/${c.id}`}>
                <StyledButton
                  disableFocusRipple
                  variant="contained"
                  className={clsx(
                    category === c.id && classes.activeDrawerMenu
                  )}
                >
                  {c.name}

                  <PlayArrowIcon
                    className={clsx(
                      classes.drawerMenuIcon,
                      classes.animatedIcon,
                      category !== c.id && classes.hidden
                    )}
                  />
                </StyledButton>
              </Link>
            </ListItem>
          ))}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Switch>
          <Route path="/item/:itemId" component={Item} />
          <Route path="/:type/category/:category" component={Home} />
          <Route path="/category/:category" component={Home} />
          <Route path="/:type" component={Home} />
          <Route path="/" component={Home} />
        </Switch>
      </main>

      <AdminLoginDialog open={open} onClose={handleClose} />

      <HiddenBar onSuccess={openAdminDialog} />
    </div>
  );
}
