import React from 'react';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'connected-react-router';
import { hot } from 'react-hot-loader/root';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import moment from 'moment';
import Routes from '../Routes';
import 'moment/locale/ko';

moment.locale('ko');

const Root = ({ store, history }) => {
  const theme = React.useMemo(
    () =>
      createMuiTheme({
        palette: {
          type: 'dark',
          primary: {
            light: '#484848',
            main: '#383639',
            dark: '#000000',
            contrastText: '#ffffff'
          },
          secondary: {
            light: '#ffff6b',
            main: '#fdd835',
            dark: '#3670a0',
            contrastText: '#000000'
          }
        }
      }),
    []
  );

  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <ThemeProvider theme={theme}>
          <Routes />
        </ThemeProvider>
      </ConnectedRouter>
    </Provider>
  );
};

Root.propTypes = {
  store: PropTypes.any.isRequired,
  history: PropTypes.any.isRequired,
};

export default hot(Root);
